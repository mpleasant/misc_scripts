---
title: "Frederick_County_Tidy"
author: "M_Pleasant"
date: "5/31/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(sf)
library(tidytransit)

```

## Read in GTFS feed ##

Enter the GTFS feed you'd like to explore below. 

```{r Read in feeds}

feed <- "E:\\MDOT_MMA\\Chapter30_Scoring\\Ch30_2019\\GTFS\\_v_Frederick_County\\Fredrick_County.zip"

gtfs <- read_gtfs(feed, geometry=TRUE)


```

```{r Mapping frequencies}

routes <- gtfs$.$routes_sf

route_frequency <- gtfs$.$routes_frequency

routes_to_map <-left_join(routes, route_frequency, by="route_id")


stops <- gtfs$.$stops_sf

stops_frequency <- gtfs$.$stops_frequency

stops_to_map <-left_join(stops, stops_frequency, by="route_id")


mapview(routes_to_map, zcol="median_headways")

mapview(stops_to_map)

```