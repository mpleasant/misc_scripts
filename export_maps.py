year = "2010"
decays = [ "HBW", "HBO" ]
activites = [ "Jobs", "Homes" ]
decay_dict = { "HBW" : "Transit Trips (Home-Based Work)", "HBO" : "Transit Trips (Home-Based Other)" }


def export_maps(year,decays,activities,outputfolder=r"C:/Users/vws1/Desktop/test_output/test.jpg"):
    for decay in decays:
        for activity in activities:
            mxd = arcpy.mapping.MapDocument("CURRENT")
            mxd.title = "{} {} Accessibility".format(year, activity)
            mxd.summary = decay_dict[decay]
            lyrs = arcpy.mapping.ListLayers(mxd)
            for lyr in lyrs:                   
                if decay in lyr.name and activity in lyr.name:
                    lyr.visible = True
            arcpy.mapping.ExportToJPEG(mxd, outputfolder)
            for lyr in lyrs:
                if decay in lyr.name and activity in lyr.name:
                    lyr.visible = False