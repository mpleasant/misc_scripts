#Chapter 30 tools (transit project study area)
#Author: Alex Bell, Renaissance Planning
#Last updated: Oct. 2018
"""
This module facilitates the designation of a project study area
for Maryland Chapter 30 scoring purposes.

Project study areas are used to define the area in which impacts of 
projects are measured.  They are dynamically determined based on the
travel mode affected by the project, the expected usership of the 
facilities affected by the project, and travel time changes wrought
by the project.  

For transit projects, the study area is defined based on transit
and highway travel time skims.  Zones overlapping project limits are
provided by the user as a list of zone IDs, and zones within user-
specified travel time tolerances by each mode are included in the 
project's study area.

- listStudyAreaZones
- mapStudyArea 

See Also
---------
gp_listStudyAreaZones
gp_mapStudyArea
"""

import arcpy
import numpy as np
import mma
import uuid

#globals

global _FIELD_TYPE_DICT, _FIELD_DTYPE_DICT

_FIELD_TYPE_DICT = {
    "String":"TEXT",
    "SmallInteger": "SHORT",
    "Integer": "LONG",
    "Single": "FLOAT",
    "Double": "DOUBLE",
    "Date": "DATE"
    }
_FIELD_DTYPE_DICT = {
    "String": "U",
    "SmallInteger": "<i4",
    "Integer": "<i4",
    "Single": "<f8",
    "Double": "<f8",
    "Date": "M" #?
    }
	
	
###################
## ARCPY HELPERS ##
###################

def _getWorkspaceType(path):
    desc = arcpy.Describe(path)
    try:
        return desc.workspaceType
    except AttributeError:
        return _getWorkspaceType(desc.path)
    

def _makeFieldName(table, new_field_name, seed=1):
    ws_type = _getWorkspaceType(table)
    if ws_type == u'FileSystem':
        new_field_name = new_field_name[:10]
    if _checkNewFieldName(table, new_field_name):
        if ws_type == u'FileSystem':
            new_field_name = new_field_name[:(10 - (len(str(seed)) + 1))] + "_" + str(seed)
        else:
            new_field_name = new_field_name + "_" + str(seed)
        return _makeFieldName(table, new_field_name, seed + 1)
    else:
        return new_field_name
    

def _checkNewFieldName(table, new_field_name):
    field_names = [f.name for f in arcpy.ListFields(table)]
    return new_field_name in field_names


def _getFieldTypeName(table, field_name):
    field = arcpy.ListFields(table, field_name)[0]
    ftype = field.type
    return _FIELD_TYPE_DICT[ftype]
	
	
def _getFieldDType(table, field_name):
    field = arcpy.ListFields(table, field_name)[0]
    ftype = field.type
    dtype = _FIELD_DTYPE_DICT[ftype]
    if dtype == 'U':
        dtype = "{}{}".format(dtype, field.length)
    return dtype
	

def _cleanUUID(uuid_obj):
    return str(uuid_obj).replace('-','_')


########################
## ANALYSIS FUNCTIONS ##
########################
	
def listStudyAreaZones(transit_skim_references, transit_time_cutoff, auto_skim_references, auto_time_cutoff,
							select_zones, output_table):
	"""
	Produces a table listing zones in the transit projet study area based 
	on travel times to the project by transit and driving.
	
	Parameters
	----------
	transit_skim_references : File (.json)
		A (list of) skim reference configuration file(s) to search for
		study area zones reachable within `transit_time_cutoff` from
		zones included in `select_zones`.
	transit_time_cutoff : Double
		The transit travel time tolerance used to determine a zone's 
		inclusion in the project study area.
	auto_skim_references : File (.json)
		A (list of) skim reference configuration file(s) to search for
		study area zones reachable within `auto_time_cutoff` from
		zones included in `select_zones`.
	auto_time_cutoff : Double
		The auto (highway) travel time tolerance used to determine a  
		zone's inclusion in the project study area.
	select_zones : [Variant...]
		A list of values corresponding to Zone IDs.  The list includes 
		all zones considered to be "within the project limits.
	output_table : ArcGIS Table
		The output table to be produced by the tool, listing all zones
		in the project area.  The table includes zones in `select_zones`
		as well as those reachable by transit within `transit_time_cutoff`
		and by auto within `auto_time_cutoff`.
		
	Returns
	--------
	None.  Outputs a table to the path specified by `output_table`.
	
	See Also
	--------
	gp_listStudyAreaZones
	mapStudyArea
	mma.Skim
	mma.jsonToSkim
	"""
	destinations = []
	
	for transit_skim_reference in transit_skim_references:
		destinations += _listDestinations(transit_skim_reference, select_zones, transit_time_cutoff)
	for auto_skim_reference in auto_skim_references:
		destinations += _listDestinations(auto_skim_reference, select_zones, auto_time_cutoff)
		
	destinations = list(set(destinations))
	
	if type(destinations[0]) is str or type(destinations[0]) is unicode:
		dt = np.dtype([("STUDY_AREA", "|S150")])
	else:
		dt = np.dtype([("STUDY_AREA", "<f8")])
		
	array = np.array([(d,) for d in destinations], dtype=dt)
	arcpy.da.NumPyArrayToTable(array, output_table)


def mapStudyArea(study_area_table, sa_zone_field, zones_fc, zone_id_field, layer_name=None, output_fc=None):
	"""
	Using a list of study area zones and a related zones feature class, 
	produces a feature layer showing the project study area. Optionally, 
	produces a feature class outlining the study area.
	
	Parameters
	-----------
	study_area_table : ArcGIS Table or Table View
		The table listing all zones included in the project study area.
	sa_zone_field : String
		The field in `study_area_table` that identifies each zone in the
		project study area.
	zones_fc : ArcGIS Feature Class or Feature Layer
		A polygon feature class having zones with ID values corresponding
		to those in `sa_zone_field`.
	zone_id_field : String
		The field in `zones_fc` that contains zone ID values.  It should
		be of the same data type as `sa_zone_field`.
	layer_name : String, optional
		A recongizable name for the feature layer to be produced.
		Default is None, indicating an auto-generated unique name
		will be applied to the output feature layer.
	output_fc : String, optional
		The full path to the output feature class to be produced, 
		outlining the project study area (dissolved zonal polygons).
		Default is None, indicating no output feature class will be
		produced.
		
	Returns
	--------
	out_layer : ArcGIS feature layer
		A feature layer showing the study area limits based on matching
		`zones_fc` features included in `study_area_table`.  Also, 
		optionally outptus a feature class outlining the study area to the
		path specified by `output_fc`.
	
	See Also
	--------
	gp_mapStudyArea
	listStudyAreaZones	
	"""
	#list zones in the table
	with arcpy.da.SearchCursor(study_area_table, sa_zone_field) as c:
		zones = sorted({r[0] for r in c})
			
	#build an expression for the zones feature class/zone id field
	field_expr = arcpy.AddFieldDelimiters(zones_fc, zone_id_field)
	quote_char = ""
	if _getFieldTypeName(zones_fc, zone_id_field) == "TEXT":
		quote_char = "'"
	expr = " OR ".join(["{} = {}{}{}".format(field_expr, quote_char, z, quote_char) for z in zones])
	
	#create a feature layer using the expression as the def query
	if not layer_name:
		layer_name = "Study_area_{}".format(_cleanUUID(uuid.uuid1()))
	out_layer = arcpy.MakeFeatureLayer_management(zones_fc, layer_name, expr)
	
	if output_fc:
		arcpy.Dissolve_management(out_layer, output_fc)
		
	return out_layer

	
######################
## ANALYSIS HELPERS ##
######################
	
def _listDestinations(skim_reference, select_zones, time_cutoff):
	destinations = []
	skim_obj = mma.jsonToSkim(skim_reference)
	if skim_obj.table_type == "arcpy":
		skim_table = "{}\\{}".format(skim_obj.path, skim_obj.table)
		skim_fields = skim_obj.getSkimFields()
		
		expr = _buildExpr(skim_obj, select_zones, time_cutoff)
		arcpy.AddMessage(expr)
		with arcpy.da.SearchCursor(skim_table, skim_fields, where_clause=expr) as c:
			for r in c:
				if skim_obj.d_field:
					d = r[skim_obj.d_idx]
				else:
					d = r[skim_obj.o_idx].split(skim_obj.delimiter, 1)[1]
				destinations.append(d)
	return list(set(destinations))

	
def _buildExpr(skim_obj, select_zones, time_cutoff):
	skim_table = "{}\\{}".format(skim_obj.path, skim_obj.table)
	zone_field= arcpy.AddFieldDelimiters(skim_table, skim_obj.o_field)
	imp_field = arcpy.AddFieldDelimiters(skim_table, skim_obj.impedance_field)
	quote_char = ""
	if _getFieldTypeName(skim_table, skim_obj.o_field) == "TEXT":
		quote_char = "'"
	comp, wildcard = " LIKE ", "%"
	if skim_obj.d_field:
		comp, wildcard = " = ", ""
	expr = " OR ".join(["{}{}{}{}{}{}".format(zone_field, comp, quote_char, zone, wildcard, quote_char) for zone in select_zones])
	expr = "(" + expr + ") AND {} <= {}".format(imp_field, time_cutoff)
	return expr
	
	

	
	
	
