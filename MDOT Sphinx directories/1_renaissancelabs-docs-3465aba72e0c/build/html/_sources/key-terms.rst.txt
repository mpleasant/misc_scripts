Multimodal Accessibility: Key Terms and Concepts
==============================================

Introduction
^^^^^^^^^^^^^^^^^^^^^^^^^^

Purposes and concept
^^^^^^^^^^^^^^^^^^^^^^^^^^

- Activities

- Zones
  
- Origin-Destination Matrix Tables (Skims)

Network vs spatial analysis
^^^^^^^^^^^^^^^^^^^^^^^^^^

The skim table
^^^^^^^^^^^^^^^^^^^^^^^^^^

Travel time decay rates
^^^^^^^^^^^^^^^^^^^^^^^^^^

Averaging accessibility scores for multiple zones
^^^^^^^^^^^^^^^^^^^^^^^^^^

1. “Weighted” average/SUMPRODUCT
2. Different scores for different populations/user groups
