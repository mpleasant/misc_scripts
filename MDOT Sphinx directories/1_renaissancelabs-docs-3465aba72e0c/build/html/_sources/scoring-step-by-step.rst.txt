
========================================
Chapter 30 Transit Accessibility Scoring
========================================

Introduction
============

The Chapter 30 Scoring Model
----------------------------

Pursuant to `Chapter 30 
<http://www.mdot.maryland.gov/newMDOT/Planning/Chapter_30_Score/Index.html>`_  
Acts of 2017 (`Senate Bill 307 <http://mgaleg.maryland.gov/2017RS/chapters_nol
n/Ch_30_sb0307E.pdf>`_ the Maryland Department of Transportation (MDOT) “shall, 
in accordance with federal transportation requirements, develop a project–based 
scoring system for major transportation projects using the goals and measures 
established under [Transportation Article 2-103.7(c)]” being considered for inclusion 
in the Consolidated Transportation Program (CTP). The transportation scoring law, 
as amended in 2017, defines a “major transportation project” as a highway or transit 
capacity project that exceeds $5,000,000, and excludes any “projects that are 
solely for system preservation.”

The Chapter 30 scoring model evaluates projects across nine goals and twenty-three
measures that were established in statute, using a combination of project
data, modeling analysis, and qualitative questionnaires. A project application 
process has been established requiring counties and municipalities to request 
major transportation projects to ensure the necessary project information and 
priorities are provided to conduct the scoring.

Among the goals and measures articulated Chapter 30 projects are “reducing 
congestion and improving commute times” (goal #3) and “equitable access to 
transportation” (goal #7). For each of these goals, the State has defined 
access to jobs as a key measure for project scoring, as shown in the 
following excerpts from the `Chapter 30 Technical Guide <http://www.mdot.maryl
and.gov/newMDOT/Planning/Chapter_30_Score/Images_and_Documents/MDOT_TechnicalG
uide_Final_12292017.pdf>`__.

.. figure:: ../images/Chapter_30_3.3.png
   :alt: 
   :align: center

.. figure:: ../images/Chapter_30_3.7.png
   :alt: 
   :align: center

Measures G3 M1 and G7 M1 both rely on assessing the change in access to jobs 
attributable to the project. This document focuses on the development of 
scores for these measures for transit capacity project applications.


What is Multimodal Accessibility Analysis (MMA)?
------------------------------------------------

.. image:: ../images/Destinations.png 
	:scale: 55%
	:alt: A graphic depicting the connections between an origin 
		to numerous destinations.
	:align: left

The Maryland Department of Transportation (MDOT) has for the past several years 
investigated emerging methods for estimating multimodal accessibility and 
applications of cumulative accessibility analyses for transportation planning. 
These efforts have yield a planning framework referred to as the Multimodal 
Accessibility (MMA) framework. MMA relies on transportation network analysis and 
land use data at a variety of scales to measure access to activities of interest 
(“jobs,” “essential services,” “education/training,” etc.) by multiple travel modes.

In concept, `MMA analysis <key-terms.html>`_ is simple: The goal is to measure travel 
times from zones of origin to zones of destination and summarize the activities 
accessible from each zone of origin. The resulting measure, however, is powerful. 
It describes how well connected each zone is to other zones, accounting for the 
distribution of activities across all zones and the travel times expected for different 
system users to reach various destinations. In short, the measure is sensitive to 
changes in land uses and transportation system performance. It provides insight into 
travel behaviors such as mode choices and can reveal differences in experienced or 
expected accessibility for different population groups, such as low-income households 
and minorities.

Other potential applications
----------------------------



What this guide covers
----------------------

Assumed knowledge
File organization
Data preparation
Network Setup
MMA toolbox application workflow
Project study area definition
Quality assurance






Assumed Knowledge
-----------------

This portion of the guide, which reviews the step-by-step process for scoring 
transit projects, assumes basic knowledge of the ArcMap interface and installation 
of MMA and GTFS to Network Dataset tools. It also assumes familiarity with Multimodal 
Accessibility concepts. Before proceeding with this portion of the guide, please 
review the "Multimodal Accessibility: Key Terms and Concepts" and "MMA Geoprocessing 
Toolbox Help" sections of this document in addition to the linked ArcGIS and GTFS 
resources.


Recommended Working Directory Structure
---------------------------------------

To organize the process of scoring multiple projects and streamline the workflow, 
it is helpful to follow a specific directory structure while shepherding Chapter 
30 projects through the accessibility scoring process. The recommended directory 
structure consists of the following folders:

**Decay_rates**
	Contains one or more decay rate tables to pass to the “Summarize Accessibility” 
	tool in the MMA geoprocessing toolbox.  The decay rates define how the value of 
	a destination diminishes as travel time to it increases. Decay rate tables are 
	provided as an a priori input to the Chapter 30 accessibility scoring process. 
	They should not be edited during project scoring.


**GTFS**
	Contains all GTFS feeds to be used in the development of the statewide base transit 
	network as well as updated/additional feeds representing specific projects.  All feeds 
	should be stored in this folder and appropriate feeds selected for the development of 
	the base and project networks during the network development phases of analysis (see 
	____ section below).


**Land_use**
	Contains feature classes representing zonal features across the study area. For Chapter 
	30 scoring, the study area is the entire state of Maryland and portions of neighboring 
	states.  Level 2 zones from the Maryland Statewide Travel Master (MSTM) and accompanying 
	socio-economic/demographic data are utilized as the standard set of zones for Chapter 30 
	scoring. They are provided as a priori inputs to the Chapter 30 accessibility scoring process. 
	Demographic and employment data generally should not be edited, unless a project application 
	is accompanied by a project-specific land use forecast.


**MMA_scores**
	Contains the table


**Networks**
	Contains base and project Network Datasets generated using the processes outlined in this 
	document.


**Project_specs**
	Contains shapefiles for each project network that include proposed route alignment and stops.


**Skims**
	Contains skim tables generated by the “Create OD Matrix (skim)” script described in section 
	________.  For skims generated exogenously, however, the analyst should run the “Create skim 
	reference” script to produce properly constructed skim reference files for those skims.


**Tools**
	Contains the MMA Geoprocessing Toolbox. These tools, used in combination with ArcGIS Network 
	Analyst Extension and GTFS tools, are used to complete the transit scoring process.



Data preparation
----------------



Maryland Statewide Transportation Model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Maryland State Highway Administration (SHA) developed and maintains the `Maryland 
Statewide Transportation Model (MSTM) <http://www.roads.maryland.gov/index.aspx?PageId=254>`_. 
to support a variety of transportation planning and system operation and performance 
applications. The MSTM is a multiresolution travel demand modeling platform providing 
consistent data on land uses and travel networks across the state at multiple scales. 
Level 1 is the coarsest scale and is primarily utilized for statewide analyses; Level 
2 is an intermediate scale suitable for regional-level analyses; and Level 3 is a 
fine-grained scale supporting local area analyses.  

The following data are utilized for Chapter 30 transit project accessibility scoring:

	- Point (centroid) and polygon feature classes representing MSTM Level 2 zones.

	- Socio-economic and demographic data summarized to MSTM Level 2 zones for the scoring horizon 
	year (currently 2040).

	- Polyline feature class representing MSTM Level 3 network features. Although the transit 
	analysis is carried out at the Level 2 scale, the Level 3 network is utilized to model the 
	access and egress to/from transit stops and stations at a sufficiently fine level of detail.


Transit Networks (GTFS Transit Schedules)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The General Transit Feed Specification (GTFS) is a standard format for storing 
and sharing open transit data, including route and schedule information.  GTFS 
feeds are collections of comma-delimited text (csv) files that provide sufficient 
information to model transit routing options by time of day for a selected service 
day (specific date or typical day of the week). The details of the tables included 
in a typical feed and the data recorded in each table are outlined `here <https://
developers.google.com/transit/gtfs/reference/>`__.
		
**Existing Service**
		
For Chapter 30 transit scoring, GTFS feeds for all transit properties in Maryland 
and neighboring jurisdictions (Washington, DC and northern Virginia, e.g.) were 
obtained from the `Transportation Resource Information Point website <http://www.
mdtrip.org/commuter-tools/developer-resources/>`__   These feeds offer the best 
available representation of currently available fixed-route transit services across 
the state and serve as the basis of the “base” transit network (see “__________________”).
All feeds were utilized “as is,” assuming the feed developers adequately validated the 
information contained in each feed.

For 2018 Chapter 30 scoring, the latest available feeds were downloaded on April 30, 2018.

+----------------------------------------------------+------+----------+----------+------------------+
| Agency                                             | Name | Start    | Date     | Date             |
+====================================================+======+==========+==========+==================+
| Allegany County Transit                            | MD   | 20160519 | 20180101 | Summer/Fall 2016 |
+----------------------------------------------------+------+----------+----------+------------------+
| Annapolis Transit                                  | MD   | 20100101 | 20191231 | 20161207         |
+----------------------------------------------------+------+----------+----------+------------------+
| BWI Thurgood Marshall Airport                      | MD   | 20160101 | 20171231 | 20161206         |
+----------------------------------------------------+------+----------+----------+------------------+
| Calvert County Public Transportation               | MD   | 20150923 | 20171231 | 20161208         |
+----------------------------------------------------+------+----------+----------+------------------+
| Carroll Transit System                             | MD   | 20160413 | 20180101 | 20161227         |
+----------------------------------------------------+------+----------+----------+------------------+
| Cecil Transit                                      | MD   | 20100101 | 20191231 | 20160614         |
+----------------------------------------------------+------+----------+----------+------------------+
| Charles County VanGo                               | MD   | 20151006 | 20171231 | 20161219         |
+----------------------------------------------------+------+----------+----------+------------------+
| Charm City Circulator                              | MD   | 20160506 | 20181231 | 5/17/2016        |
+----------------------------------------------------+------+----------+----------+------------------+
| Delmarva Community Transit                         | MD   | 20150923 | 20171231 | 20161020         |
+----------------------------------------------------+------+----------+----------+------------------+
| Harford Transit LINK                               | MD   | 20151001 | 20171231 | 20161128         |
+----------------------------------------------------+------+----------+----------+------------------+
| Maryland Transit Administration                    | MD   | 20100101 | 20191231 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+
| Montgomery County MD Ride On                       | MD   | 20100101 | 20191231 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+
| Ocean City Transportation                          | MD   | 20161124 | 20171231 | 20170707         |
+----------------------------------------------------+------+----------+----------+------------------+
| Queen Anne's County Ride                           | MD   | 20160308 | 20171231 | 20161221         |
+----------------------------------------------------+------+----------+----------+------------------+
| Regional Transportation Agency of Central Maryland | MD   | 20160601 | 20180301 | 20161115         |
+----------------------------------------------------+------+----------+----------+------------------+
| Shore Transit                                      | MD   | 20160318 | 20200101 | Summer 2016      |
+----------------------------------------------------+------+----------+----------+------------------+
| St. Mary's Transit System                          | MD   | 20150923 | 20171231 | 20161221         |
+----------------------------------------------------+------+----------+----------+------------------+
| The Bus of Prince George's County                  | MD   | 20160925 | 20171231 | 20161221         |
+----------------------------------------------------+------+----------+----------+------------------+
| TransIT Services of Frederick County               | MD   | 20100101 | 20191231 | 20130128         |
+----------------------------------------------------+------+----------+----------+------------------+
| Washington County Transit                          | MD   | 20160512 | 20180101 | 20161221         |
+----------------------------------------------------+------+----------+----------+------------------+
| DC Circulator                                      | DC   | 20100101 | 20191231 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+
| WMATA                                              | DC   | 20100101 | 20191231 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+
| Alexandria Transit Company (DASH)                  | VA   | 20100101 | 20191231 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+
| Arlington Transit                                  | VA   | 20100101 | 20191231 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+
| Fairfax Connector                                  | VA   | 20100101 | 20191231 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+
| Fairfax CUE                                        | VA   | 20080701 | 20200101 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+
| Loudon County Transit                              | VA   | 20100101 | 20191231 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+
| Virginia Railway Express                           | VA   | 20100101 | 20191231 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+
| Winchester Transit                                 | VA   | 20100101 | 20191231 | [no data]        |
+----------------------------------------------------+------+----------+----------+------------------+

**Future service**

When infrastructure improvements are being considered for funding, it is 
necessary to either update an existing or build a new GTFS feed.  Therefore, 
specific descriptive information on the proposed service is needed. Attributes 
needed to document the new service include: transit stop locations, estimated 
departure times, service hours, etc... The most recent and authoriatite planning 
documents shold be identified for the development of these GTFS feeds.

After proposed service documentation is identified, GTFS files need to be 
edited or developed to represent the proposed service. The matrix below outlines 
possible infrastructure improvements and the associated actions required for 
a GTFS feed.

SCENARIOS FOR UPDATING GTFS 

+---------------------+--------------------------+-----------+
| Improvement         | GTFS attributes affected | Action    |
+=====================+==========================+===========+
| Alignment change    | Stops, Stop Times        | Update    |
+---------------------+--------------------------+-----------+
| Service change      | Calendar                 | Update    |
+---------------------+--------------------------+-----------+
| Headway improvement | Stop Times, Frequencies  | Update    |
+---------------------+--------------------------+-----------+
| New route           | All                      | Build new |
+---------------------+--------------------------+-----------+
| Route deviation     | All                      | Build new |
+---------------------+--------------------------+-----------+

The following section offers an example for building a new GTFS feed for a proposed BRT route along Veirs Mill Rd (MD 586). Project specifications were collected from the Maryland Transit Administration (MTA). Additionally, some assumptions were developed to determine the approximate distance between proposed stop locations. Project specifications include:

1.	An anticipated peak hours headway of six minutes ;
2.	An assumed travel speed of 20 MPH, and 
3.	Twenty-four (24) total stops, 12 in each direction.

PROPOSED STOPS

+-----+----------------------------------------------+
|  1  | Approximate Locations                        |
+=====+==============================================+
|  2  | Montgomery College                           |
+-----+----------------------------------------------+
|  3  | Rockville Metrorail Station                  |
+-----+----------------------------------------------+
|  4  | Veirs Mill Rd at MD 28/Norbeck Road          |
+-----+----------------------------------------------+
|  5  | Veirs Mill Rd at Broadwood Drive             |
+-----+----------------------------------------------+
|  6  | Veirs Mill Rd at Twinbrook Parkway           |
+-----+----------------------------------------------+
|  7  | Veirs Mill Rd at Aspen Hill Road             |
+-----+----------------------------------------------+
|  8  | Veirs Mill Rd at Parkland Drive              |
+-----+----------------------------------------------+
|  9  | Veirs Mill Rd at Randolph Road               |
+-----+----------------------------------------------+
|  10 | Veirs Mill Rd at MD 185/Connecticut Avenu    |
+-----+----------------------------------------------+
|  11 | Veirs Mill Rd at Newport Mill Road           |
+-----+----------------------------------------------+
|  12 | Veirs Mill Rd at MD 193/University Boulevard |
+-----+----------------------------------------------+
|  13 | Wheaton Metrorail Station                    |
+-----+----------------------------------------------+


Network setup
----------------------------------------------------

Utilize GTFS for Network Analyst toolbox
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Networks datasets need to be created to represent both base and build scenarios. This requires the 

Tool download http://esri.github.io/public-transit-tools/AddGTFStoaNetworkDataset.html

Network dataset preparation: https://github.com/Esri/public-transit-tools/blob/master/add-GTFS-to-a-network-dataset/UsersGuide.md

**Step 1: Generate Transit Lines and Stops**

Translates GTFS data to line and point features classes
Builds SQL database of transit schedule within working geodatabase

**Step 2: Generate Stop-Street Connectors**

Snap stop features to non-transit links
Create lines that connect transit stop locations to snapped locations to allow interaction between transit lines and access/egress network

**Step 3: Get Network EIDs**

After creating and building the network dataset, this script prepares the network for use in Network Analyst

For more information:

Tool download http://esri.github.io/public-transit-tools/AddGTFStoaNetworkDataset.html

Network dataset preparation: https://github.com/Esri/public-transit-tools/blob/master/add-GTFS-to-a-network-dataset/UsersGuide.md


Data management for network scenarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Including new services
2. Toggling services as parameters


MMA toolbox application workflow
----------------------------------------------------------------

Introduce Chapter 30 toolbox
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



- Series of geoprocessing scripts for use in ArcGIS

	- Create build and no-build multimodal networks using the “Add GTFS to network dataset” toolbox

	- Process MMA scores for each scenario using “MMA” toolbox

	- Define project study areas using “Chapter30” toolbox


.. figure:: ../images/chapter_30_toolboxes.png



Step-by-step tool usage
----------------------------------------------------------------

To calculate change in accessibility due to a transit or non-motorized project, the following workflow is used.

.. figure:: ../images/chapter_30_transit_Workflow.png

Typical workflow:


1. Decay curves 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Setup decay rates (if necessary) using “Mange decay rates” tool


2. Update network datasets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


3. Create skims (OD Matrix)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create OD matrices by running “Create skims” tool in MMA toolbox. Specify a range of travel times representing a travel period (“AM Peak,” e.g.).


4. Produce average travel times skim
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run “Average travel times” tool to get typical OD travel times within the travel period. This tool synthesizes the multiple skims created curing the "Create skims" step into a single skim reflecting typical conditions for the period. Note: if OD matrices have been created exogenously, use “Create skim reference” tool to prepare the data for use in other MMA tools


5. Summarize accessibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Use “Summarize accessibility” tool in the MMA toolbox to process skims and zonal data to produce accessibility scores by zone.


6. Define project study area
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The project study area is based on travel time to the zones in which the project is implemented – any zones within 45 minutes by transit (in the project build scenario) are part of the study area as are any zones within 15 minutes by driving (based on MSTM highway skims).

7. Calculate change in accessibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For change in accessibility versus the base, run “Calculate change in accessibility” tool in MMA toolbox.

- Use “Calculate change in accessibility” tool to ascertain differences between the build and no build accessibility results

- Use the “Calculate weighted average” tool to summarize the change in accessibility for the project study area (study area features produced separately)

- For average scores within the study area…
- Select zones based on study area definition (tool in separate toolbox)
- Run “Weighted average” tool in MMA toolbox, weighting by general and disadvantaged population


8. Average score within study area
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


9. Report changes in accessibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Quality assurance
--------------------------------------------------------------------------


GTFS feed validation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Location loading
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Routing problems
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To confirm the build scenario network additions and or modificatoins are property configured, a network routing test should be conducted to identify the shortest path between two points. For these tests, beginning and ending points are identified near the termini of the project. The purpose of this test is to confirm that the network is utilizing the new path created by the project. If the build and no-build networks are configured property, the build scenario route should traverse the proposed network segment. This test may also shows a travel time reduction. Use the Network Analyst. See section ________ for documentation on using the Network Analyst extension for this task.

Service area problems (travel time contours)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A service area analysis is conducted under build and no-build scenarios for each project. The result of this analysis is a map of travel time isochrones for a single location within the project study area. A simple check that the isochrones expand as the project provides additional service and that the expansion is intuitive given the nature of the project is sufficient to confirm that the project is appropriately integrated into the base multimodal network.

To condocut this test, Network Analyst the following settings should be use

		
.. figure:: ../images/Service_area_analysis_settings.JPG 

.. figure:: ../images/Service_Area_Network_Locations.JPG 

.. figure:: ../Service_Area_Polygon_Generation.JPG


Chapter 30 Transit Project Standard Report
---------------------------------------------------

Transit scoring is documented using a standard report form, which could contain the following document structure:

Chapter I.	Introduction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1.	Overview of Project

Chapter II.	Coding Assumptions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Alignment
2. Attributes
3. Modifications of Existing GTFS Feeds

Chapter III. Network Review Results
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Network Dataset Configuration Review
2. Connectivity Tests
3. Shortest Path

Chapter IV.	Reasonableness of results
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Extent of Study Area
2.	Travel Time Contours to Project
3.	MMA Results

Chapter V.	Mapping and Final Results
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Network (Project Links and Stops)
2. Build vs No Build
3. Project Study Area
4. Positive and Negative Accessibility Changes

