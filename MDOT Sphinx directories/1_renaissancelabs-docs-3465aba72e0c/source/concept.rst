Multimodal Accessibility Core Concepts
======================================
**What is accessibility?**
	We all have important destinations to get to in our daily life. These include
	work, school, shopping, recreation, and other activities.  Accessibility 
	measures our ability to reach these destinations.

	.. image:: ../images/destinations.png
	   :scale: 60 %
	   :alt: alternate text

	The number of activities we can reach depends on the location we are in, the 
	travel options available to us, and how far we are willing to travel.  The 
	types of activities that are important to us depend on our age, lifestyle, 
	relationships, personal and professional obligations, and many other factors. 

	.. figure:: ../images/LocalAccess.png
	   :scale: 40 %
	   :alt: alternate text

	   Only local destinations are accessible by walking

	.. figure:: ../images/RegionalAccess.png
	   :scale: 40 %
	   :alt: alternate text

	   Planes, trains, and automobiles make far-away destinations accessible

	
**Why is accessibility important?**

	Explain why.