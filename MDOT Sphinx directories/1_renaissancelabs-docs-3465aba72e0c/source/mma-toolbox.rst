MMA Geoprocessing Toolbox Help
==============================

MMA
---

.. automodule:: mma
    :members:
    :undoc-members:
    :show-inheritance:

Ch30Tools
---------

.. automodule:: Ch30Tools
    :members:
    :undoc-members:
    :show-inheritance:

gp_CreateAverageMatrix
----------------------

.. automodule:: gp_CreateAverageMatrix
    :members:
    :undoc-members:
    :show-inheritance:

gp_listStudyAreaZones
---------------------

.. automodule:: gp_listStudyAreaZones
    :members:
    :undoc-members:
    :show-inheritance:

gp_mapStudyArea
---------------

.. automodule:: gp_mapStudyArea
    :members:
    :undoc-members:
    :show-inheritance: