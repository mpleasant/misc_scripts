
Skims
=====================================================

A skim file describes the travel time and distance between a set of origins and destinations. The file includes a record for each origin and destination combination. 
Depending on the number of origins and destinations, the number of records the file(s) contain can be quite large. The skims for your project are created with the **Create OD matrix (skim)** tool. 

Data contained in the skim files are organized into the following columns:

=======================   ===========     =============   =================  =====================
Name                       OriginID        Destination     Total_Walk_Time    Total_Walk_Distance
=======================   ===========     =============   =================  =====================
Origin-Destination pair
=======================   ===========     =============   =================  =====================    