Decay rates
===========

Travel time decay concepts
^^^^^^^^^^^^^^^^^^^^^^^^^^

Travel time decay refers to the idea that destinations nearby are more relevant
to travelers than destinations that are far away.  As travel time to an activity
increases, the likelihood of traveling to it decreases.

Travel time decay components
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* curve form - negative exponential formula
* constant
* coefficient
* minimum/maximum travel time
* lower/upper bounds of decay calcualtion
* exclude values less than the minimum time
