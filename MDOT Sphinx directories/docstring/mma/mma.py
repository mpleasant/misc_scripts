#MULTIMODAL ACCESSIBILITY
"""
MMA
~~~

This module facilitates many procedures required to develop multimodal
accessibility (mma) scores in ArcGIS.

The MMA module allows users to generate network analysis problems to 
create (series of) travel time skims and process those skims against
related zone land use data and travel time decay curves to generate
accessibility scores for each zone.  

Create skims
Summarize accessibility
Decay rates
Skim
SkimSet

Notes
-----
Accessibility scores describe the quantity of "activities" reachable 
within a given travel time by a given mode, discounted by typical travel
time tolerances for that mode.  Key steps in generating accessibility 
scores include:
- Configure decay rates
- Create skim (OD matrix)
- Summarize accessibility

Dependencies: arcpy (ArcGIS Network Analyst extension must be installed, 
licensed, and enabled), numpy, pandas
"""

import arcpy
import numpy as np
import pandas as pd
import sqlite3
import time
import csv
import json
import datetime

#check out extensions
arcpy.CheckOutExtension("Network")

#create centroids
def createZonalCentroids(zones_fc, fields, output_fc, where_clause="",
						 skip_nulls=False, null_value=None):
	sr = arcpy.Describe(zones_fc).spatialReference
	shape_fields = ["SHAPE@X", "SHAPE@Y"]
	fields += shape_fields
	array = arcpy.da.FeatureClassToNumPyArray(zones_fc, fields, where_clause, sr,
											  skip_nulls=skip_nulls,
											  null_value=null_value)    
	arcpy.da.NumPyArrayToFeatureClass(array, output_fc, shape_fields, sr)


class Skim(object):
	def __init__(self, path, table, impedance_field,
				 o_field, d_field=None, delimiter=None):
		"""
The Skim class desribes an OD matrix (skim) table to aid in 
accessibility processing.
	
The Skim object epxoses the location, key fields, and type (arcpy
vs text) of a skim table for reading and parsing during 
accesibility analysis.
		
Parameters
----------
path : String
	The workspace in which the skim table is stored.
table : String
	The name of the skim table (with file extension, if any).
impedance_field : String
	The field in the skim table that provides the operative
	impedance value for accessibility analysis.
o_field : String
	The field in the skim table that provides the origin
	value or the field that provides the full origin-
	destination pair information.
d_field : String, optional
	The field in the skim table that provides the destination
	value.  (Default is None, which implies that `o_field` gives
	the full origin-destination pair information.)
delimiter : String, optional
	If `o_field` gives the fill origin-destination pair
	information, `delimeter` indicates the character sequance
	by which to split `o_field` values to get separate origin
	and destination ID's. (Default is None, implying o_field
	and d_field have been provided.)

Attributes
----------
path : String
	See "Parameters"
table : String
	See "Parameters"
impedance_field : String
	See "Parameters"
o_field : String
	See "Parameters"
d_field : String
	See "Parameters"
delimiter : String
	See "Parameters"
o_idx : Integer
	Expected index of `o_field` in a cursor navigating `table`
d_idx : Integer
	Expected index of `d_field` in a cursor navigating `table`
imp_idx : Integer
	Expected index of `impedance_field` in a cursor navigating
	`table`
table_type : {"arcpy", "text"}
	Indicates the appropriate method for navigating the table
	for processing.  Probable deprecation.
	
Methods
--------
setFieldIdxs()
	Sets the Skim object's field index attributes based on its 
	current field name attributes
setTableType()
	Sets the Skim object's table_type attribute based on its 
	current path and table attributes.
getSkimFields()
	Returns a list of skim fields to pass to cursor for processing.
	
See Also
---------
SkimSet: Collection of skim objects for batch accessibility 
processing.
skimToJson : storage of a Skim object in a JSON config file.
jsonToSkim : creation of a Skim object from a JSON config file.
		"""
		self.path = path
		self.table = table
		self.impedance_field = impedance_field
		self.o_field = o_field
		self.d_field = d_field
		self.delimiter = delimiter
		self.setTableType()
		self.setFieldIdxs()

	def setOField(self, o_field):
		try:
			old_field = self.o_field
			self.setFieldIdxs()
		except:
			self.o_field = old_field
			Traceback.print_exc()

	def setDField(self, d_field):
		try:
			old_field = self.d_field
			self.setFieldIdxs()
		except:
			self.d_field = old_field
			traceback.print_exc()

	def setImpedanceField(self, impedance_field):
		try:
			old_field = self.impedance_field
			self.setFieldIdxs()
		except:
			self.impedance_field = old_field
			traceback.print_exc()

	def setFieldIdxs(self):
		"""
		Sets the Skim objects o_idx, d_idx, and impedance_idx attributes
		based on its current o_field, d_field, and impedance_field 
		attributes.
		"""
		if self.table_type == "arcpy":
			self.o_idx = 0            
			if self.d_field:
				self.d_idx = 1
				self.imp_idx = 2
			else:
				self.d_idx = None
				self.imp_idx = 1
		else:
			with open("{}\\{}".format(self.path, self.table), 'r') as csvfile:
				reader = csv.reader(csvfile)
				header = reader.next()
				self.o_idx = header.index(self.o_field)
				if self.d_field:
					self.d_idx = header.index(self.d_field)
				else:
					self.d_idx = None
				self.imp_idx = header.index(self.impedance_field)

	def setPathAndTable(self, path, table):
		old_path = self.path[:]
		old_table = self.table[:]
		try:
			self.path = path
			self.table = table
			self.setTableType()
		except:
			self.path = old_path
			self.table = old_table
			traceback.print_exc()

	def setTableType(self):
		"""
		Sets the Skim object's table_type attribute based on the its 
		current path and table attributes.
		"""
		try:
			#if self.table can be read in arcpy, self.table_type = arcpy
			field_check = arcpy.ListFields("{}\\{}".format(self.path, self.table), self.o_field, 'String')
			if field_check:
				#if self.o_field is a string field, set the od_field type to by a string of equal length
				self.od_field_type = '|S{}'.format(field_check[0].length)
			else:
				#if self.o_field is not a string field, set the od_field type to be an integer
				self.od_field_type = '<i4'
			self.table_type = "arcpy"
		except RuntimeError:
			#if self.table cannot be read in arcpy, try it as a csv            
			with open("{}\\{}".format(self.path, self.table), 'r') as csvfile:
				reader = csv.reader(csvfile) #,quoting=csv.QUOTE_NONNUMERIC)???
				header = reader.next() #the csv is assumed to have a header on the first row
				check_col = header.index(self.o_field)
				row = reader.next()
				if type(row[check_col]) is str or type(row[check_col]) is unicode:
					#if the value in the first row of the table for the o_field column is a string...
					self.od_field_type = '|S255' 
				else:
					#assume the od field type will be integer
					self.od_field_type = '<i4'
			self.table_type = "text"
		
	def getSkimFields(self):
		"""
		Returns a list of skim fields to pass to cursor for processing.
		"""
		if self.d_field:
			return [self.o_field, self.d_field, self.impedance_field]
		else:
			return [self.o_field, self.impedance_field]  


def _skimToDict(skim_obj):
	skim_dict = {"path": skim_obj.path, "table": skim_obj.table,
				 "impedance_field": skim_obj.impedance_field,
				 "o_field": skim_obj.o_field, "d_field": skim_obj.d_field,
				 "delimiter": skim_obj.delimiter,
				 "table_type": skim_obj.table_type,
				 "imp_idx": skim_obj.imp_idx, "o_idx": skim_obj.o_idx,
				 "d_idx": skim_obj.d_idx,
				 "od_field_type": skim_obj.od_field_type}
	return skim_dict


def skimToJson(skim_obj, output_file):
	"""
	Stores a Skim object in a JSON config file.
	
	Parameters
	----------
	skim_obj : Skim object
		A Skim object stores details of a skim table that facilitate
		accessibility summarization.
	output_file : String
		The full path to store the output file.  If just the file name is
		passed, the file will be saved in the current working directory.
		
	Returns
	--------
	None
	
	See Also
	--------
	jsonToSkim : creation of a Skim object from a JSON config file.
	Skim : Skim object for accessibility processing.
	"""
	skim_dict = _skimToDict(skim_obj)
	if output_file[-5:].lower() != ".json":
		output_file = output_file + ".json"
	with open(output_file, 'wb') as f:
		json.dump(skim_dict, f)


def jsonToSkim(in_file):
	"""
	Creates a Skim object for accessibility processing from a JSON config
	file.
	
	Parameters
	-----------
	in_file : string
		The file name in the current working directory or full path to the
		JSON config file from which to generate the Skim object.
		
	Raises
	------
	KeyError
		If the provided `in_file` is not a valid Skim config file.
	
	See Also
	---------
	skimToJson : storage of a Skim object in a JSON config file.
	Skim : Skim object for accessibility processing.
	"""
	if in_file[-5:].lower() != ".json":
		in_file = in_file + ".json"
	with open(in_file, 'rb') as f:
		skim_dict = json.load(f)
	return _dictToSkim(skim_dict)


def _dictToSkim(skim_dict):
	path = skim_dict["path"]
	table = skim_dict["table"]
	impedance_field = skim_dict["impedance_field"]
	o_field = skim_dict["o_field"]
	d_field = skim_dict["d_field"]
	delimiter = skim_dict["delimiter"]
	return Skim(path, table, impedance_field, o_field,
				d_field=d_field, delimiter=delimiter)


class SkimSet(object):
	def __init__(self): #
		"""
		The SkimSet class groups one or many Skim objects for use in batch
		accessibility summarization.
		
		Attributes
		----------
		skims : [skim_obj_1, skim_obj_2, ...skim_obj_n]
			The Skim objects that comprise the SkimSet			
			
		Methods
		--------
		addSkim(skim_obj)
			Adds a skim object to the SkimSet's `skims` list.
		removeSkim(path, table)
			Removes a skim object from the SkimSet's `skims` list based
			on the skim object's `path` and `table` attributes.
		
		See Also
		---------
		Skim : Skim object for accessibility processing.
		"""
		#self.name = name
		self.skims = []
		#self.select_zones = []
		#self.select_zones_type = "origins"

	def addSkim(self, skim_obj):
		"""
		Adds a skim_obj to the SkimSet's `skims` list.
		"""
		self.skims.append(skim_obj)

	def removeSkim(self, path, table):
		"""
		Remove a skim_obj to the SkimSet's `skims` list based on its 
		`path` and `table` attributes.
		"""
		idx = None
		for i in range(len(self.skims)):
			skim = self.skims[i]
			s_path = skim.path
			s_table = skim.table
			if s_path == path and s_table == table:
				idx = i
		if idx is not None:
			del self.skims[idx]

	#def setSelectZones(self, zone_list):
	#	"""zone_list = [zone1, zone2, ... zoneN]"""
	#	#use unique zones in the list
	#	self.select_zones = list({zone for zone in zone_list})
	

def createSkims(network, impedance_attribute, o_features, o_name, d_features, d_name, 
				output_workspace, analysis_name,
				cutoff=None, number_of_ds=None, restrictions=None, u_turns="ALLOW_UTURNS",
				group_origins=False, group_features=None, group_id_field=None,
				group_selection_method="INTERSECT", group_selection_radius="",
				use_network_locations=False,
				o_SourceID='', o_SourceOID='', o_PosAlong='', o_SideOfEdge='', o_SnapX='', o_SnapY='', o_Distance='',
				d_SourceID='', d_SourceOID='', d_PosAlong='', d_SideOfEdge='', d_SnapX='', d_SnapY='', d_Distance='',
				search_criteria="",
				tolerance='5000 Meters', match=True, exclude_restricted=True, query_layer='', search_query='',
				use_time_of_day=False, day_of_week="Today", time_window_start="", time_window_end="", time_window_increment=1):
	"""Create a (set of) OD travel time matrix tables.

	Parameters
	----------
	network : ArcGIS Network Dataset or Network Dataset Layer
		Path to an ArcGIS network dataset or name of an ArcGIS network 
		dataset layer in the active data frame.
	impedance_attribute : String
		The name of the impedance attribute in `network` to be used in
		determining shortest paths between `o_features` and `d_features`.
	o_features : ArcGIS Feature Class or Feature Layer
		Path to an ArcGIS point feature class or name of an ArcGIS point
		feature layer in the active data frame.
	o_name : String
		Field in `o_features` to use as the origin ID value when
		tabulating travel times.
	d_features : ArcGIS Feature Class or Feature Layer
		Path to an ArcGIS point feature class or name of an ArcGIS point
		feature layer in the active data frame (can be the same FC/FL as
		`o_features`).
	d_name : String
		Field in `d_features` to use as the destination ID value when
		tabulating travel times.	
	output_workspace : ArcGIS Workspace or string
		ArcGIS Workspace object (file folder, geodatabase, etc.) or 
		string representing the path to the workspace. Location where
		output skim tables will be stored.
	analysis_name : String
		A string of characters to include in the names of output files
		to differentiate them from other files produced in the same
		`output_workspace.`  Short strings of 7 characters or fewer
		are recommended.
	cutoff : Double, optional
		The maximum `impedance_attribute` value from `o_features` beyond 
		which `d_features` will not be tabulated in the skim.
		(Default is None, which implies no maximum `impedance_attribute`
		value.)
	number_of_ds : Integer, optional
		The maximum number of `d_features` to find for each `o_feature.`
		(Default is None, which finds all origins within `cutoff`.)
	restrictions : String, optional
		(Default is None.) The restriction attributes in `network` to honor 
		when finding shortest paths as a semi-colon-separated string. 
		"Oneway;PedesetrianOnly" e.g.
	u_turns : {"ALLOW_UTURNS", "NO_UTURNS", "ALLOW_DEAD_ENDS_ONLY",
		"ALLOW_DEAD_ENDS_AND_INTERSECTIONS_ONLY" }
		The u-turn policy to honor when	finding shortest paths. 
	
	Returns
	-------
	None
		Nothing is retured by the function.  It will output one or more
		skim tables to the `output_workspace` with names reflecting
		`analysis_name`, `group_id_field` (if any), and the time window
		(if any).  For each output skim, a skim reference configuration
		file (.json) will also be created in 
		`output_workspace`\_skim_references
	
	Other Parameters
	----------------	
	group_origins : Boolean, optional 
		(Default is False.)  True indicates that origins should be
		grouped for processing.  Grouping limits the number of features
		included in a given OD matrix tabulation to manage memory and 
		output file sizes.
	group_features : ArcGIS Feature Class or Feature Layer, optional  
		(Deafult is None.)  If `group_origins` is True, origins will be
		grouped based on the relationship of features in `o_features`
		to features in `group_features`.
	group_id_field : String, optional
		(Default is None.) Name of the field in `group_features` that
		organizes the grouping of `o_features`.  Distinct values in 
		this field will be included in output file names to relate
		each skim table to its origin group.
	group_selection_method : String, optional
		The spatial relationship to apply when grouping `o_features` based
		on `group_features`. (Default is "INTERSECT", which relates 
		`o_features` to `group_features`  that they intersect. All ArcGIS
		overlap_types are valid.)
	group_selection_radius : Linear Unit (String), optional
		The distance to search around `group_features` for testing their
		spatial relationship to `o_features`. (Default is "", which
		implies a strict relationship among featues [i.e., no search
		radius].  Example: "100 Feet")
	use_network_locations : Boolean, optional	
		(Default is False.)  True indicates `o_features` and `d_features`
		will load on to the `network` using pre-calculated values stored
		in various fields stored in their respective attribute tables.
		False indicates `o_features` and `d_featues` will load on to the
		`network` based on spatial criteria (this takes longer and can
		lead to inconsistencies in loading locations).
	o_SourceID : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the SourceID field in `o_features`
	o_SourceOID : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the SourceOID field in `o_features`
	o_PosAlong : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the PosAlong field in `o_features` 
	o_SideOfEdge : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the SideOfEdge field in `o_features`
	o_SnapX : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the SnapX field in `o_features`
	o_SnapY : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the SnapY field in `o_features`
	o_Distance : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the Distance field in `o_features`
	d_SourceID : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the SourceID field in `d_features`
	d_SourceOID : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the SourceOID field in `d_features`
	d_PosAlong : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the PosAlong field in `d_features` 
	d_SideOfEdge : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the SideOfEdge field in `d_features`
	d_SnapX : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the SnapX field in `d_features`
	d_SnapY : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the SnapY field in `d_features`
	d_Distance : String, optional
		(Default is "".)  If `use_network_locations` is True, provide
		the name of the Distance field in `d_features`
	search_criteria : String, optional
		If `use_network_locations` is False, list the network sources and 
		snapping points on which `o_features` and `d_features` may load 
		in a semi-colon-separated string. Example: 
		"Streets Midpoint;Streets Endpoint"
	tolerance : Linear Unit (String), optional
		If `use_network_locations` is False, specify the maximum distance
		from a `network` source listed in `search_citeria` to search for
		`o_features` and `d_features` for loading.  Features beyond the 
		`tolerance` will be ignored during analysis. (Default is "5000 
		Meters".)
	match : Boolean, optional
		If `use_network_locations` is False, specify how to select loading
		locations based on `network` sources listed in `search_criteria`.
		If `match` is True, features will load on the closest valid source.
		This is the deafult. 
		If `match` is False, features will honor the priority of `network`
		sources implied by the order in which they are listed in
		`search_criteria`, loading on the closest in a given priority 
		group. 
	exclude_restricted : Boolean, optional 
		If `use_network_locations` is False, specify whether `o_features`
		and `d_features` can be loaded on excluded `network` features.  
		Excluded features are those honored as listed in `restrictions`.
		If `exclude_restricted` is True, excluded features will be ignored.
		This is the default.
		If `exclude_restricted` is False, some locations may load on
		restricted features.
	search_query : String, optional
		If `use_network_locations` is False, optionally specify which
		`network` features are available for loading.  This is an 
		expression string that further constrains loading beyond the 
		limits set by `search_criteria`, `match`, and `exclude_restricted`.
		(Default is "".)
	use_time_of_day : Boolean, optional
		(Default is False.) True indicates that `network` is time-enabled
		and the user desires skims for a specific day and time.
	day_of_week : {"Today", "Monday", "Tuesday", "Wednesday", "Thursday",
	"Friday", "Saturday"}, optional
		If `use_time_of_day` is True, indicate the day of week to analyze.
	time_window_start : DateTime, optional
		If `use_time_of_day` is True, indicate the initial time to analyze.
		Multiple skims may be produced based on the `time_window_end` and
		`time_window_increment` values.
	time_window_end : DateTime, optional
		If `use_time_of_day` is True, indicate the final time to analyze.
		Multiple skims may be produced based on the `time_window_start`
		and `time_window_increment` values.
	time_window_increment : Double, optional
		If `use_time_of_day` is True, indicate the interval at which to
		increment the time so that multiple skims will be produced for
		every interval of `time_window_increment` between 
		`time_window_start`	and `time_window_end`.
		
	See Also
	--------
	jsonToSkim : creation of a Skim object from a JSON config file.
	skimToJson : storage of a Skim object in a JSON config file.
	Skim : desribes an OD matrix (skim) table to aid in accessibility 
		processing		
	SkimSet: Collection of skim objects for batch accessibility processing.	
	"""
	#setup outputs
	out_ws_type = arcpy.Describe(output_workspace).workspaceType
	if out_ws_type == u'FileSystem':
		f_ext = '.dbf'
		imp_attr = "Total_{}".format(impedance_attribute)[:10]
	else:
		f_ext = ''
		imp_attr = "Total_{}".format(impedance_attribute)
	
	#rework select inputs    
	search_criteria, expressions = _formatSearchCriteria(search_criteria, network, query_layer, search_query)
	actual_criteria = _searchCriteraToString(search_criteria)    
	_printMessage("Reformatted search criteria: \n{}".format(actual_criteria))
	actual_expression = _expressionsToString(expressions)

	#other supporting derived variables
	accumulation = _listAccumulationAttributes(network, impedance_attribute)
	d_field_mappings, o_field_mappings = _ODFieldMappings(d_name, o_name)
	d_location_fields = [d_SourceID, d_SourceOID, d_PosAlong, d_SideOfEdge, d_SnapX, d_SnapY, d_Distance]
	o_location_fields = [o_SourceID, o_SourceOID, o_PosAlong, o_SideOfEdge, o_SnapX, o_SnapY, o_Distance]    

	#create network problem
	_printMessage("creating OD problem")    
	OD = arcpy.MakeODCostMatrixLayer_na(network, "OD", impedance_attribute, default_cutoff=cutoff,
								   default_number_destinations_to_find=number_of_ds,
								   accumulate_attribute_name=accumulation, UTurn_policy=u_turns,
								   restriction_attribute_name=restrictions, hierarchy="NO_HIERARCHY", 
								   output_path_shape="NO_LINES")
	od_analysis_layer = OD.getOutput(0)	

	#add destinations
	_addLocationsToProblem("OD", "Destinations", d_features, d_field_mappings, tolerance,
						   actual_criteria, exclude_restricted, actual_expression,
						   use_network_locations, d_location_fields, match)

	#add origins, solve matrix, and save results
	if group_origins:
		o_layer = arcpy.MakeFeatureLayer_management(o_features, "o_layer")
		group_count = 0
		groups = _listUniqueValues(group_features, group_id_field)
		for group in groups:
			out_file_name = "{}_skims_{}".format(analysis_name, group)
			_printMessage("Selecting origins in group {}".format(group))
			#select origin features
			_selectGroupFeatures(group, group_features, group_id_field, o_layer,
						 overlay_selection_method=group_selection_method,
								 search_radius=group_selection_radius)
			#add selected origins to OD problem
			_addLocationsToProblem(od_analysis_layer, "Origins", o_layer, o_field_mappings, tolerance,
								   actual_criteria, exclude_restricted, actual_expression,
								   use_network_locations, o_location_fields, match)
			#solve and export (iterate by time of day if needed)
			_solveOdAndExport(od_analysis_layer, imp_attr, output_workspace, out_file_name, f_ext, group=group, 
								use_time_of_day=use_time_of_day, day_of_week=day_of_week,
								time_window_start=time_window_start, time_window_end=time_window_end,
								time_window_increment=time_window_increment)
	else:
		out_file_name = "{}_skims".format(analysis_name)
		#add origins to OD problem
		_addLocationsToProblem(od_analysis_layer, "Origins", o_features, o_field_mappings, tolerance,
							   actual_criteria, exclude_restricted, actual_expression,
							   use_network_locations, o_location_fields, match)
		#solve and export (iterate by time of day if needed)
		_solveOdAndExport(od_analysis_layer, imp_attr, output_workspace, out_file_name, f_ext, 
							use_time_of_day=use_time_of_day,day_of_week=day_of_week,
							time_window_start=time_window_start, time_window_end=time_window_end,
							time_window_increment=time_window_increment)

		
def _solveOdAndExport(od_analysis_layer, imp_attr, output_workspace, out_file_name, f_ext, group=None, 
						use_time_of_day=False, day_of_week="", time_window_start="", time_window_end="", 
						time_window_increment=1):
	skim_folder = _getSkimObjectsFolder(output_workspace)
	if group:
		_printMessage("Solving OD matrix for origins in group {}".format(group))
	else:
		_printMessage("Solving OD matrix for all origins")
	#setup times for solves
	if use_time_of_day:						
		DAY_DATE_DICT = {
				"Today": "12/30/1899",
				"Monday": "1/1/1900",
				"Tuesday": "1/2/1900",
				"Wednesday": "1/3/1900",
				"Thursday": "1/4/1900",
				"Friday": "1/5/1900",
				"Saturday": "1/6/1900",
				"Sunday": "12/31/1899"
				}
		if day_of_week.lower() != "use specific date":		
			date = datetime.datetime.strptime(DAY_DATE_DICT[day_of_week], '%m/%d/%Y')
			time_window_start = time_window_start.replace(year=date.year, month=date.month, day=date.day)
			time_window_end = time_window_end.replace(year=date.year, month=date.month, day=date.day)		
		delta = datetime.timedelta(minutes = time_window_increment)
		current_dt = time_window_start
		times = []
		while current_dt <= time_window_end:		
			times.append(current_dt)
			current_dt += delta
	else:
		times = [None] #""
	
	#iterate over times, solve, and export
	sublayer_names = arcpy.na.GetNAClassNames(od_analysis_layer)
	lines_layer_name = sublayer_names["ODLines"]
	lines_sublayer = arcpy.mapping.ListLayers(od_analysis_layer, lines_layer_name)[0]

	for time in times:
		#setup file name and messaging
		_printMessage("...{}".format(time))
		out_file_name_t = "{}_{}{}".format(out_file_name, time, f_ext)
		out_file_name_t = out_file_name_t.replace(":", "")
		out_file_name_t = out_file_name_t.replace("-", "")
		out_file_name_t = out_file_name_t.replace(" ", "_")
		#update OD matrix time settings
		od_solver_properties = arcpy.na.GetSolverProperties(od_analysis_layer)
		od_solver_properties.timeOfDay = time
		#solver OD problem
		arcpy.Solve_na(od_analysis_layer, "SKIP", "TERMINATE")
		count = int(arcpy.GetCount_management(lines_sublayer).getOutput(0))
		_printMessage("... ...success ({} rows)".format(count))

		_printMessage("exporting output {}\\{}".format(output_workspace, out_file_name_t))
		arcpy.TableToTable_conversion(lines_sublayer, output_workspace, out_file_name_t)
		#save skim specs 
		skim = Skim(output_workspace, out_file_name_t, imp_attr, "Name",
					d_field=None, delimiter=" - ")
		skim_file = _getSkimFileName(skim_folder, out_file_name_t)
		skimToJson(skim, skim_file)
			
def _getSkimFileName(out_folder, out_file_name):
	if out_file_name[-4:] == ".dbf":
		out_file_name = ".".join([out_file_name.rsplit('.', 1)[0], 'json'])
	else:
		out_file_name += ".json"
	return "\\".join([out_folder, out_file_name])
				

def _getSkimObjectsFolder(workspace):
	desc = arcpy.Describe(workspace)
	if desc.workspaceType != u"FileSystem":
		workspace = workspace.rsplit('\\', 1)[0]
		return _getSkimObjectsFolder(workspace)
	else:
		arcpy.CreateFolder_management(workspace, "_skim_objects")
		return "\\".join([workspace, "_skim_objects"])
					
		
def _addLocationsToProblem(od_analysis_layer, location_type, in_features, field_mappings,
						   tolerance, search_criteria, exclude_restricted,
						   search_queries, use_location_fields=False, location_fields=[],
						   match=True):    
	_printMessage("Loading {} from {}".format(location_type, in_features))   
	#setup field mappings
	if use_location_fields:                          
		locationMappings = _locationFieldMappings(location_fields)               
		field_mappings+= "; %s" % locationMappings

	#setup other location parameters
	if match:
		match_type = "MATCH_TO_CLOSEST"
	else:
		match_type = "PRIORITY"
	if exclude_restricted:
		exclude_restricted = "EXCLUDE"
	else:
		exclude_restricted = "INCLUDE"    
	_printMessage(", ".join([str(field_mappings), str(tolerance), str(search_criteria),
						 str(match_type), str(exclude_restricted), str(search_queries)]))

	#add locations to OD problem
	arcpy.AddLocations_na(od_analysis_layer, location_type, in_features, field_mappings, tolerance,
						  "", search_criteria, match_type, "CLEAR", "NO_SNAP", "",
						  exclude_restricted, search_queries)
	#result = arcpy.GetCount_management("\\".join([analysis_layer, location_type]))
	#_printMessage(int(result.getOutput(0)))
	return


def _locationFieldMappings(location_fields):
	SourceID, SourceOID, PosAlong, SideOfEdge, SnapX, SnapY, Distance = location_fields
	out_str = "SourceID %s #;" % SourceID
	out_str += "SourceOID %s #;" % SourceOID
	out_str += "PosAlong %s #;" % PosAlong
	out_str += "SideOfEdge %s #;" % SideOfEdge
	out_str += "SnapX %s #;" % SnapX
	out_str += "SnapY %s #;" % SnapY
	out_str += "Distance %s #" % Distance
	return out_str


def _formatSearchCriteria(search_criteria, network, query_layer, query_expression):
	#search_criteria passed from gp interface: "Source_fc snap_preference; Source_fc2 snap_preference"
	#reformatted as [[Source_fc, snap_preference],[Source_fc2, snap_preference]]
	criteria_dict = {}
	expressions = []
	if search_criteria != "":
		search_criteria = search_criteria.split(";")
		for crit in search_criteria:
			source = crit.split(" ",1)[0].strip().replace("'", "")
			snap = crit.split(" ",1)[1].strip().replace("'", "")
			#source = crit.rsplit(" ",1)[0][1:]
			#snap = crit.rsplit(" ", 1)[1][:-1]
			_printMessage(", ".join([source, snap]))            
			if source in criteria_dict:
				criteria_dict[source]=criteria_dict[source]+"_"+snap
			else: criteria_dict[source] = snap
			if source == query_layer:
				expression = [source, query_expression]
			else:
				expression = [source, "#"]
			expressions.append(expression)
		del search_criteria
		#_printMessage(criteria_dict)
		search_criteria = []    
		desc = arcpy.Describe(network)
		netsources = desc.sources
		for netsource in netsources:
			if netsource.name in criteria_dict:
				critlist = [netsource.name,criteria_dict[netsource.name]]
			else:
				critlist = [netsource.name,"NONE"]
			search_criteria.append(critlist)
	if expressions == []:
		expressions = "#"
	return search_criteria, expressions


def _searchCriteraToString(search_criteria):
	crit_str = ""
	for sc in search_criteria:
		this_c = "'{}' {};".format(sc[0], sc[1])
		crit_str += this_c
	return crit_str[:-1]


def _expressionsToString(expressions):
	expr_str = ""
	if expressions != "#":
		for ex in expressions:
			this_e = "'{}' {};".format(ex[0], ex[1])
			expr_str += this_e
		return expr_str[:-1]
	else:
		return expressions
				

def _listAccumulationAttributes(network, impedance_attribute):
	accumulation = []
	desc = arcpy.Describe(network)
	attributes = desc.attributes
	for attribute in attributes:
		if attribute.name == impedance_attribute:
			continue
		elif attribute.usageType == "Cost":
			accumulation.append(attribute.name)
	return accumulation


def _ODFieldMappings(dname, oname):
	dFieldMappings = "Name " + dname + " #"
	oFieldMappings = "Name " + oname + " #"
	return dFieldMappings, oFieldMappings


def _selectGroupFeatures(group_id, group_features, group_id_field, zones_layer,
						 overlay_selection_method="INTERSECT", search_radius=""):
	#select zones in one feature class based on their spatial relationship
	#to selected zones in another feature class. Used in mma to select zones for loading into
	#and OD problem to manage processing loads/output file sizes
	expression = arcpy.AddFieldDelimiters(group_features, group_id_field)
	if type(group_id)is str or type(group_id) is unicode:
		expression += r"= '%s'" % group_id
	else:
		expression += r"= %f" % group_id
	group_layer = arcpy.MakeFeatureLayer_management(group_features, "__ZONE_GROUP_{}".format(group_id),
													where_clause=expression)        
	arcpy.SelectLayerByLocation_management(zones_layer, overlay_selection_method,
										   group_layer, search_radius, "NEW_SELECTION")
	return


class Decay(object):
	def __init__(self, name, const, coef, min_impedance=0, max_impedance=float('inf'),
				 excl_less_than_min=False, excl_greater_than_max=True,
				 lbound=0.0, ubound=1.0, desc = ""):
		"""
		The Decay class defines how to discount activities at different
		destinations based on the travel time from the origin.
		
		The Decay object assumes a (negative) exponential formula that 
		defines a cureve for discounting activities at destinations
		based on their travel time from the origin (in minutes).  The 
		primary decay formula is:
		.. math:: const * (e^{coef * minutes }) 
		
		It can be modified such that the value of `minutes` and/or the 
		results of the formula can be constrained by minimum/maximum
		values.
		
		Parameters
		----------
		name : String
			A short name to identify the decay rate.  The name is added
			to fields in the tables generated by the `summarizeAccessibility`
			function, so a short string of a few characters is recommended.
		const : Float
			The constant term in the exponential formula used to define
			the decay curve.
		coef : Float
			The constant term in the exponential formula used to define
			the decay curve.
		min_impedance : Float, optional
			The minimum impedance value to be evaluated as the `minutes`
			term in the Decay formula.  Values less than `min_impedance`
			are ignored if `excl_less_than_min` is True.  Otherwise, 
			values less than `min_impedance` are treated as the 
			`min_impedance` value. (Default is 0, which implies no
			minimum value.)
		max_impedance : Float, optional
			The maximum impedance value to be evaluated as the `minutes`
			term in the Decay formula.  Values greater than `max_impedance`
			are ignored if `excl_greater_than_max` is True.  Otherwise, 
			values greater than `max_impedance` are treated as the 
			`max_impedance` value. (Default is float('inf'), which implies 
			no maximum value.)			
		excl_less_than_min : Boolean, optional
			Flag defining how to treat `minutes` values less than
			`min_impedance`.  If True, values less than `min_impedance`
			are ignored.  If False, values less than `min_impedance`
			are treated as `min_impedance`.  Default is False.
		excl_greater_than_max : Boolean, optional
			Flag defining how to treat `minutes` values greater than
			`max_impedance`.  If True, values greater than `max_impedance`
			are ignored.  If False, values greater than `max_impedance`
			are treated as `max_impedance`.  Default is True.
		lbound : Float, optional
			Minimum value to return when evaluating the decay funtion
			specified by the Decay object with respect to a given value
			of `minutes`.  Default is None.
		ubound : Float, optional
			Maximum value to return when evaluating the decay funtion
			specified by the Decay object with respect to a given value
			of `minutes`.  Default is None.
		desc : String, optional
			A longer description of the decay curve being defined.  The
			`desc` parameter offers more detail than can be conveyed in
			the `name` parameter.
		
		Attributes
		----------
		name : String
			See "Parameters"
		const : Float
			See "Parameters"
		coef : Float
			See "Parameters"
		min_impedance : Float
			See "Parameters"
		max_impedance : Float
			See "Parameters"
		excl_less_than_min : Boolean
			See "Parameters"
		lbound : Integer
			See "Parameters"
		ubound : Integer
			See "Parameters"
		desc : String
			See "Parameters"
			
		Methods
		--------
		setFieldIdxs()
			Sets the Skim object's field index attributes based on its 
			current field name attributes
		setTableType()
			Sets the Skim object's table_type attribute based on its 
			current path and table attributes.
		getSkimFields()
			Returns a list of skim fields to pass to cursor for processing.
			
		See Also
		---------
		summarizeAccessibility: Collection of skim objects for batch accessibility 
		processing.
		decayToJson : storage of a Decay object in a JSON config file.
		jsonToDecay : creation of a Decay object from a JSON config file.
		"""
		self.name = name
		self.desc = desc
		self.const = const
		self.coef = coef
		self.min_impedance = min_impedance
		self.max_impedance = max_impedance
		self.excl_less_than_min = excl_less_than_min
		self.excl_greater_than_max = excl_greater_than_max
		self.lbound = lbound
		self.ubound = ubound
	
	def evaluate(self, minutes):
		"""
		Estimates the decay weight to return based on the Decay object's 
		attributes and a float value `minutes`
		
		Parameters
		-----------
		minutes : Float
			A non-negative numeric value representing travel time in 
			minutes.
			
		Returns
		--------
		decay_factor : Float
			A weighting factor the reflects the discount to apply when
			weighting destinations in accessibility analyses based on
			the Decay object's attributes and the travel time between
			an O-D pair in minutes.
		"""
		if minutes <0:
			raise ValueError("Value of minutes must be >= 0")
		if self.min_impedance < minutes:
			if self.excl_less_than_min:
				return 0
			else:
				minutes = self.min_impedance
		if self.max_impedance is not None and \
		self.max_impedance != float('inf'):
			if self.excl_greater_than_max:
				return 0
			else:
				minutes = self.max_impedance
		decay_factor = self.const * np.exp(self.coef * minutes)
		if self.lbound:
			decay_factor = max(decay_factor, self.lbound)
		if self.ubound:
			decay_factor = min(decay_factor, self.ubound)
		return decay_factor
		

def decayToJson(decay_obj, output_file):
	"""
	Stores a Decay object in a JSON config file.
	
	Parameters
	----------
	decay_obj : Decay object
		A Decay object defines how to discount activities at different
		destinations based on the travel time from the origin.
	output_file : String
		The full path to store the output file.  If just the file name is
		passed, the file will be saved in the current working directory.
		
	Returns
	--------
	None
	
	See Also
	--------
	jsonToDecay : creation of a Decay object from a JSON config file.
	Decay : Decay object for weighted accessibility processing.
	"""
	_printMessage("saving decay object '{}' to file ({})".format(decay_obj.name, output_file))
	decay_dict = _decayToDict(decay_obj)
	if output_file[-5:].lower() != ".json":
		output_file = output_file + ".json"
	with open(output_file, 'wb') as f:
		json.dump(decay_dict, f)
		

def _decayToDict(decay_obj):
	decay_dict = {"name": decay_obj.name, "desc": decay_obj.desc,
				  "const": decay_obj.const, "coef": decay_obj.coef,
				  "min_impedance": decay_obj.min_impedance,
				  "max_impedance": decay_obj.max_impedance,
				  "excl_less_than_min": decay_obj.excl_less_than_min,
				  "lbound": decay_obj.lbound, "ubound": decay_obj.ubound}
	return decay_dict


def jsonToDecay(in_file):
	"""
	Creates a Decay object for accessibility processing from a JSON config
	file.
	
	Parameters
	-----------
	in_file : string
		The file name in the current working directory or full path to the
		JSON config file from which to generate the Decay object.
		
	Raises
	------
	KeyError
		If the provided `in_file` is not a valid Decay config file.
	
	See Also
	---------
	decayToJson : storage of a Decay object in a JSON config file.
	Decay : Decay object for weighted accessibility processing.
	"""
	if in_file[-5:].lower() != ".json":
		in_file = in_file + ".json"
	with open(in_file, 'rb') as f:
		decay_dict = json.load(f)
	return _dictToDecay(decay_dict)


def _dictToDecay(decay_dict):
	name = decay_dict.get("name", "")
	desc = decay_dict.get("desc", "")
	const = decay_dict.get("const", 1)
	coef = decay_dict.get("coef", 0)
	min_impedance = decay_dict.get("min_impedance", 0)
	max_impedance = decay_dict.get("max_impedance", float('inf'))
	excl_less_than_min = decay_dict.get("excl_less_than_min", False)
	lbound = decay_dict.get("lbound", 0.0)
	ubound = decay_dict.get("ubound", 1.0)
	return Decay(name, const, coef, min_impedance=min_impedance, max_impedance=max_impedance,
				 excl_less_than_min=excl_less_than_min, lbound=lbound, ubound=ubound,
				 desc=desc)
	

###########################                                                                                
# SUMMARIZE ACCESSIBILITY #
###########################
def summarizeAccessibility(skim_set_obj, lu_table, lu_table_id, 
							lu_table_activity_fields, out_table, 
							decays=[]):
	"""
	Summarizes access to activities at desinations based on skims in a 
	SkimSet objects, a land use table, and a list of Decay objects.
	
	Parameters
	-----------
	skim_set_obj : SkimSet
		As SkimSet object comprised of the Skim objects defining the skim
		tables to referenc in accessibility summarization.
	lu_table : ArcGIS Table or TableView
		A table that organizes land use data by zones with an ID field
		that corresponds to values in each skim table's destination ID
		values.
	lu_table_id : String
		The field in `lu_table` that identifies each zone.  Values in this
		field should correspond to values in each skim table's destination
		ID values.
	lu_table_activity_fields: [String,...]
		A list of field names representing the activities at each 
		(destination) zone to summarize in the accessibility tabulation.
	out_table : ArcGIS Table
		The output table containing accessiblity summarization results.
	decays : [Decay_obj,...]
		A list of Decay objects to be applied in the summarization of 
		activities (in `lu_table_activity_fields`).		
	
	Returns
	-------
	None
		Nothing is retured by the function.  It will produce an 
		`output_table` containing the accessibility summaries. The table
		will contain a field with each distinct origin ID listed in the
		skim tables listed in `skim_set_obj`. It will also have fields
		summarizing all activities listed in `lu_table_activity_fields`
		(having the same field names as those in that list) as well as
		sets of the same fields with decayed summaries with names pre-
		fixed by each decay rate's `name` attribute.
	
	See Also
	---------
	Skim : desribes an OD matrix (skim) table to aid in accessibility 
	processing.		
	SkimSet : Collection of skim objects for batch accessibility processing. 
	Decay : Decay object for weighted accessibility processing. 
	"""
	
	start_time = time.time()
	#dump lu_table to np array, lu table represents destination land uses
	lu_fields = [lu_table_id] + lu_table_activity_fields
	lu_array = arcpy.da.TableToNumPyArray(lu_table, lu_fields, skip_nulls=True)

	#build dtype lists for reference throughout   
	decay_dtype_list = [('DEC_{}'.format(decay_obj.name), '<f8') for 
							decay_obj in decays]
	activity_dtype_list = [(str(lu_table_activity_field), '<f8') for 
							lu_table_activity_field in lu_table_activity_fields]
	wtd_act_dtype_list = [('{}_{}'.format(decay_obj.name,lu_table_activity_field), '<f8')
							  for lu_table_activity_field in lu_table_activity_fields
								   for decay_obj in decays]
	
	#iterate over skim objects in skim_set to create a master table of summarized activity
	#   reachable from each origin zone in the combined set of skims
	summed_dfs = []
	for skim_obj in skim_set_obj.skims:
		_printMessage('processing skim: {}'.format(skim_obj.table))
		skim_dtype_list = [('origin', skim_obj.od_field_type), 
						('destination', skim_obj.od_field_type),
					   (str(skim_obj.impedance_field), '<f8')]        
		skim_dtype = np.dtype(skim_dtype_list + decay_dtype_list)        
		
		#process the skim - each skim will be summarized and it's summary table (data frame) will be appended
		#    to the summed_dfs list.  The summaries in the summed_dfs list will be combined in a later step.
		if skim_obj.table_type == "arcpy":
			summed_dfs.append(_processArcpySkim(skim_obj, skim_dtype, lu_array,
												   lu_table_activity_fields, skim_set_obj.select_zones, decays))
		else:
			summed_dfs.append(_processTextSkim(skim_obj, skim_dtype, lu_array,
												  lu_table_activity_fields, skim_set_obj.select_zones, decays))

	#"stack" summed arrays and export final summary    
	sum_fields = lu_table_activity_fields + [wtd_act_field[0] for wtd_act_field in wtd_act_dtype_list]
	out_dtype = np.dtype([('origin', skim_obj.od_field_type)] + activity_dtype_list + wtd_act_dtype_list)
	if len(summed_dfs) > 1:
		out_df = _combineAndSummarizeDfs(summed_dfs, 'origin', sum_fields)        
	else:
		out_df = summed_dfs[0]
	out_array =  _makeArrayFromSummarizedDf(out_df, out_dtype)        
	arcpy.da.NumPyArrayToTable(out_array, out_table)
	end_time = time.time()
	_printMessage("elapsed time: {} minutes".format((end_time - start_time)/60.0))
			

def _processArcpySkim(skim_obj, skim_dtype, lu_array, lu_table_activity_fields, select_zones = [], decays=[]):      
	skim_fields = skim_obj.getSkimFields()
	with arcpy.da.SearchCursor("{}\\{}".format(skim_obj.path, skim_obj.table), skim_fields) as c:
		return _processIterRows(c, skim_obj, skim_dtype,
								lu_array, lu_table_activity_fields, select_zones, decays)  


def _processTextSkim(skim_obj, skim_dtype, lu_array, lu_table_activity_fields, select_zones = [], decays=[]):
	#open the text file as a csv for reading
	with open("{}\\{}".format(skim_obj.path, skim_obj.table), 'r') as csvfile:
		reader = csv.reader(csvfile)
		header = reader.next() #the text file is assumed to have a header in the first row
		return _processIterRows(reader, skim_obj, skim_dtype,
								lu_array, lu_table_activity_fields, select_zones, decays)
	

def _processIterRows(iter_rows, skim_obj, skim_dtype, lu_array, lu_table_activity_fields,
					 select_zones = [], decays=[], max_rows=500000):
	#Max rows for "tiling" the skim (processing chunks at a time to conserve memory)
	# Default is 1M rows to be processed at a time    
	counter = 1    

	#set dummy row for populating the skim array(s), empty set of out_rows, and a list to store summed arrays
	dummy_row = [0 for _ in skim_dtype.descr]
	out_rows = []
	summed_dfs = []

	for row in iter_rows:
		#get origin, destination, and impedance values for each row
		impedance = row[skim_obj.imp_idx]
		if skim_obj.d_field:
			origin = row[skim_obj.o_idx]
			destination = row[skim_obj.d_idx]			
		else:
			origin = row[skim_obj.o_idx].split(skim_obj.delimiter,1)[0]
			destination = row[skim_obj.o_idx].split(skim_obj.delimiter,1)[1]

		#update the dummy row to populate a "blank" set of out_rows
		dummy_row[0] = origin
		dummy_row[1] = destination
		dummy_row[2] = impedance
		out_rows.append(tuple(dummy_row))

		#check counter vs. max rows, summarize rows in chunks based on max rows
		if counter % max_rows == 0:
			_printMessage("...procesed {} rows".format(counter))
			summed_dfs.append(
				_processArray(
					np.array(out_rows, skim_dtype), lu_array, lu_table_activity_fields, decays, select_zones))
			out_rows = []

		counter += 1

	#process remaining records in out_rows    
	summed_dfs.append(
		_processArray(
			np.array(out_rows, skim_dtype), lu_array, lu_table_activity_fields, decays))

	#combine summed arrays
	wtd_act_fields = ['{}_{}'.format(decay_obj.name,lu_table_activity_field)
							  for lu_table_activity_field in lu_table_activity_fields
								   for decay_obj in decays]
	
	sum_fields = lu_table_activity_fields + wtd_act_fields
	_printMessage('...stacking and summarizing combined rows')
	
	
	if  len(summed_dfs) > 1:
		out_df = _combineAndSummarizeDfs(summed_dfs, 'origin', sum_fields)
	else:
		this_df = summed_dfs[0]
		if this_df.index.name != 'origin':
			this_df.set_index('origin', inplace=True)
		out_df = this_df
	_printMessage(str(out_df.head()))
	return out_df
	

def _processArray(skim_array, lu_array, lu_table_activity_fields, decays, select_zones=[]):
	impedance_field = skim_array.dtype.names[2]
	# apply decay curves  
	for decay_obj in decays:
		decay_field = 'DEC_{}'.format(decay_obj.name)
		#calculate raw decay, then adjust times below the min, above the max, and apply lbound/ubound                
		skim_array[decay_field] = decay_obj.const * np.exp(skim_array[impedance_field] * decay_obj.coef)        
		#min check
		if decay_obj.excl_less_than_min:
			skim_array[decay_field][np.where(skim_array[impedance_field] < decay_obj.min_impedance)] = 0
		else:
			skim_array[decay_field][np.where(skim_array[impedance_field] < decay_obj.min_impedance)] = decay_obj.const * np.exp(decay_obj.min_impedance * decay_obj.coef)
		#max check
		skim_array[decay_field][np.where(skim_array[impedance_field] > decay_obj.max_impedance)] = 0
		#bounds check
		skim_array[decay_field][np.where(skim_array[decay_field] < decay_obj.lbound)] = decay_obj.lbound
		skim_array[decay_field][np.where(skim_array[decay_field] > decay_obj.ubound)] = decay_obj.ubound
	
	# lookup/weight/summarize activities at destination
	return _joinAndWeightLuData(skim_array, lu_array, decays, select_zones)


def _joinAndWeightLuData(skim_array, lu_array, decays, select_zones=[]):
	lu_fields = list(lu_array.dtype.names)
	lu_table_id = lu_fields.pop(0)
	sum_fields = lu_fields[:]
	out_dt_list = [skim_array.dtype.descr[0]]
	for lu_field in lu_fields:
		out_dt_list.append((lu_field, '<f8'))

	#convert arrays to data frames in pandas and join
	df_skim = pd.DataFrame(skim_array)
	
	#trim df_skim if using select zones
	if select_zones:
		df_select_zones = pd.DataFrame(select_zones, columns=['zone'])
		df_skim = pd.merge(df_skim, df_select_zones, 'inner', left_on='origin', right_on='zone')        
		
	df_lu = pd.DataFrame(lu_array)
	df_skim = pd.merge(df_skim, df_lu, 'inner', left_on='destination', right_on=lu_table_id)    

	#calculate weighted activities
	for lu_field in lu_fields:
		for decay_obj in decays:
			sum_field = '{}_{}'.format(decay_obj.name, lu_field)
			sum_fields.append(sum_field)
			out_dt_list.append((sum_field, '<f8'))
			df_skim[sum_field] = df_skim['DEC_{}'.format(decay_obj.name)] * df_skim[lu_field]
	
	#summarize/group by
	df_sum = df_skim.groupby(['origin'])[sum_fields].sum()    
	
	#return array
	return df_sum.reset_index()

	
def _summarizeArray(array, case_field, sum_fields):
	summed_rows = []
	case_idx = array.dtype.names.index(case_field)
	dt_list = [array.dtype.descr[case_idx]] + [(sum_field, '<f8') for sum_field in sum_fields]
	cases = np.unique(array[case_field])
	for case in cases:        
		summed_row = [case] + [np.sum(
								array[sum_field][np.where(array[case_field] == case)]
										) 
									 for sum_field in sum_fields]
		summed_rows.append(tuple(summed_row))    
	return np.array(summed_rows, np.dtype(dt_list))


def _combineAndSummarizeDfs(df_list, groupby_field, sum_fields):
	df_concat = pd.concat([df for df in df_list if not df.empty])
	try:
		df_sum = df_concat.groupby([groupby_field])[sum_fields].sum()
	except KeyError:
		df_concat.reset_index(inplace=True)
		df_sum = df_concat.groupby([groupby_field])[sum_fields].sum()
	return df_sum


def _makeArrayFromSummarizedDf(df_sum, dtype):
	try:
		out_array = np.array([tuple(row) for row in df_sum.values], dtype)
	except ValueError:
		out_array = df_sum.reset_index().values
		out_array = np.array([tuple(row) for row in out_array], dtype)
	return out_array




##############
### PROJECTS #
##############
##class Project(object):
##    def __init__(self, name, links_ref_fc="", zones_ref_fc="",
##                 links_list=[], zones_list=[], summary_area_type='links'):
##        self.name = name
##        self.links_ref_fc = self.setLinksRefFc(links_ref_fc)
##        self.zones_ref_fc = zones_ref_fc
##        self.links_list = links_list
##        self.zones_list = zones_list
##        self.summary_area_type = summary_area_type
##        self.links_specs = {}
##        # {"id_fields": [id_field1,... id_fieldn],
##        #  "id_values": [[id_val1x,... id_valnx],[id_val1y,...idvalny]],
##        #  "attributes":
##        self.zones_specs = {} 
##        #nodes projects?
##
##    def setLinksRefFc(self, links_ref_fc):
##        #validate
##        try:
##            desc = arcpy.Describe(links_ref_fc)
##            if desc.shapeType == u"Polyline":
##                self.links_ref_fc = desc.catalogPath
##            else:
##                raise ValueError("links_ref_fc must be a Polyline Feature Class")
##        except:
##            traceback.print_exc() 
##
##    def setZonesRefFc(self, zones_ref_fc):
##        #validate
##        try:
##            desc = arcpy.Describe(zones_ref_fc)
##            if desc.shapeType == u"Polygon":
##                self.zones_ref_fc = desc.catalogPath
##            else:
##                raise ValueError("links_ref_fc must be a Polygon Feature Class")
##        except:
##            traceback.print_exc()
##
##    def





########################
# SUPPORTING FUNCTIONS #
########################

def _printMessage(message):
	print message
	arcpy.AddMessage(message)
	

def _listUniqueValues(table, field):    
	with arcpy.da.SearchCursor(table, field) as c:
		return sorted({r[0] for r in c})
	

	   
