MMA Geoprocessing Toolbox Help
=====================================================

Introduction 
------------------------

Overview of accessibility (link to Key Terms and Concepts document)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Objectives
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**MMA toolbox objectives**

**Help article objectives**

Roster of tools in MMA toolbox with brief descriptions of each
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tool requirements
------------------------

1. ArcGIS 10.2 or later
2. Network analyst extension**
3. Python 2.7.12 or later (but not Python 3.X):

**Modules**

1. Pandas (v X.X or later)
2. Json (standard)
3. Numpy (included with Pandas)

Installation with ArcGIS instructions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Install python 2.7.12 or later (this should include the PIP module)
2. PIP install pandas (this will download and install the pandas module as well as dependencies like numpy)
3. Install ArcGIS

Do not re-install python, just point to your existing python installation from the steps above
Authorize Network Analyst (required for Create OD Matrix tool only)

Typical MMA analysis workflow
------------------------

Diagram
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Generalize steps
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Network dataset preparation
------------------------

Links to ESRI NA help
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Links to GTFS feed specification details
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Links to GTFS for Network Analyst help
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tool-by-tool help
------------------------


Create OD matrix (skim)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Use the “Create OD Matrix (skim)” tool in the MMA toolbox to create a transit skim.  This tool interface consists of five sections, which are described in detail below.

**Field 1. Network specification**

Provide the details of the network dataset supporting the analysis. Recommended variables are indicated in the screenshot below.

.. figure:: ../images/skimming_tool_1.png 

**Field 2. OD locations**

	a. Origin locations
	
	Specify the features that will represent origin locations and how to group origins (if desired or needed for memory management). Recommended variables are indicated in the screenshot below.
	
	.. figure:: ../images/skimming_tool_2.png 
	
	b. Destination locations	
	
	Specify the features that will represent destination locations. Recommended variables are indicated in the screenshot below.
	
	.. figure:: ../images/skimming_tool_3.png 
	
**Field 3. Location loading preferences**

Define how OD features will load onto the network using pre-calculated network location fields or spatial analysis. Recommended variables are indicated in the screenshot below.

.. figure:: ../images/skimming_tool_4.png 

**Field 4. Output**

Specify where output table(s) will be stored and how to name them. Recommended variables are indicated in the screenshot below.

.. figure:: ../images/skimming_tool_5.png 

**Field 5. Time of day**

Specify the time of day for the analysis and setup iterative runs covering multiple times throughout the day. Recommended variables are indicated in the screenshot below.

.. figure:: ../images/skimming_tool_6.png 

**Field 6. Results**

This tool produces multiple tables with estimated travel times between zones for each specified departure time. Travel times and accessibility vary by time of day.

.. figure:: ../images/Chapter_30_od_skims_output.png


Create skim reference
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manage decay rates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Summarize accessibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. figure:: ../images/summerize_1.png

**Skim reference files**

Skim reference files (JSON format) store metadata about skim tables. These files are parsed by the “Summarize Accessibility” tool and define which fields to use for identifying origin/destination zones and travel time information.  The tool can handle multiple skim reference files.  Multiple files should be used when:

- Analyzing multiple modes at the same geographic scale (run in series)
- A single study area requires multiple skim tables due to its size (i.e., if origins were “grouped” in the “Create OD Matrix (skim)” tool) (do not run in series)
- Accessibility is being summarized for multiple departure times (run in series)

**Run in series (optional)**

When the Summarize Accessibility tool is “run in series,” a distinct output table will be created for each skim reference file provided in the list at top.  Otherwise, all results will be “collapsed” into a single table.

**Land use table**

Accessibility is summarized based on land use data that define the number of activities in each zone.  For each origin zone, the number of activities at reachable destinations is summarized.  The table of land uses providing these zonal data must be specified here.

**Land use table ID field**

In the skims files, the origin and destination zones are stored using zone ID values.  The corresponding ID values for the land use table are specified here.  The data type for the land use table ID field should match the data type for OD data stored in the skims tables (if OD values are stored as text, the land use table ID field should be a text field also, e.g.)

**Land use table activity fields**

Activities to summarize are listed here.  Multiple fields may be selected.  In the example, access to “education” and “health care” jobs will be summarized (as well as all jobs, which is not pictured due to the length of the fields list)

**Apply decay rates**

Decay rates define the value of time, weighting destinations nearby as being more valuable than destinations farther away.  They can also be used to create “time slices” of accessibility. Decay rates are managed using the “manage decay rates” tool.  Provide desired decay rate files (JSON format) here to utilize decay weighting in the accessibility analysis.  The tool will always produce “un-decayed” summaries of activities accessible and will produce “decayed” results with column headings corresponding to the decay rate names associated with each decay rate configuration (JSON) file.

**Output table (if not running in series)(optional)**

Outputs are managed in the bottom portion of the tool interface.  

- If the Summarize Accessibility tool is not “run in series,” it will produce a single table, which the user specifies in the “output table” field.  
- If the Summarize Accessibility tool is “run in series,” it will produce multiple tables.  In this case, the user specifies and output workspace where output tables will be stored and an “analysis name.”  The tool will produce multiple tables, each having the analysis name plus a suffix linking the results to the corresponding skim file.

The tool yields one or more tables of accessibility results that can be joined to a zone feature class for mapping

Average travel times
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Separate transit skims are created for specific departure times as specified in section 5 of section 2. above, titled "Create skims (OD Matrix)."  All of the resulting skims should be stored in a single folder for calculating average transit travel times during the travel period.  The folder with the time-of-day results is provided here, and the specific tables to summarize are selected from the list below.

.. figure:: ../images/Chapter_30_produce_average_travel_time_skim.png

Attributes of the time-of-day skims are listed to ensure the correct columns are referenced when developing the period-wide average skim.  

The “Name” field should be “Name” if the skims were developed using the “Create OD Matrix” tool.  The name field anticipates values structured as “[origin name] – [destination name]”
The “impedance weight” field is the field storing time-of-day travel time estimates between each zone pair.

Reference zones and ID information are provided so that origin and destination zones can be properly indexed during the averaging process.  These zones should reflect those used during the skim generation process.


Calculate change in accessibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculate weighted average
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
