.. mma documentation master file, created by
   sphinx-quickstart on Sun Sep 30 06:15:48 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mma's documentation!
===============================

This is a test of the emergency broadcasting system...


.. toctree::
   :maxdepth: 4
   :caption: Contents:

   mma
   Ch30Tools
   gp_createAverageMatrix
   gp_listStudyAreaZones
   gp_mapStudyArea


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`